<?php namespace ferreteria\classloader;
/*
  FILE: modloader.php
  PURPOSE: class for managing library loading
  NOTES:
    Libaries and Modules don't actually know about each other. Loading a module loads the library definition file (typically @libs.php
      or config-libs.php), which may cause (additional) libraries to be registered. When a library is loaded, it adds more Modules.
    To debug module-loading, call fcCodeModule::SetDebugMode(TRUE);
  HISTORY:
    2009-07-05 Trying to make this usable: shortened method names, added Path(), AddLog()
    2009-10-06 IsLoaded()
    2010-10-06 Log shows if update is not a change
    2011-09-15 Add() now has a "force" parameter to override an existing entry.
      By default, it does not replace existing entries.
    2012-04-18 class autoload registration
    2013-01-02 substantially rewritten - new clsModule class interface; clsLibMgr deprecated
    2013-08-28 moved clsModule to modloader.php (discarded clsLibMgr)
      The clsModule and clsLibMgr ways of doing things are incompatible, so they should be separate.
      Adding class management to clsModule -- renaming methods to specify class or module
    2013-09-12 Added function indexing ability.
      ...which should probably be deprecated in favor of static class functions.
    2015-03-14 Adding support for "libraries" (clsLibrary), i.e. collections of modules
      whose location is known but which are only loaded into the index on request
    2016-10-01 some documentation and reorganization; debugging function
    2017-01-01 renaming clsModule -> fcCodeModule, clsLibrary -> fcCodeLibrary
      Moved into Ferreteria folder, so it will be included in the repository
      rather than needing to be managed separately. It's unlikely to be used without Ferreteria --
      but if it is, it can always be extracted.
    2017-12-17 support for CLI mode in debugging output
    2019-09-15 Namespace, some rewriting and renaming...
      class fcCodeLibrary -> cLibrary
      class fcCodeModule -> cModule
      file modloader.php -> ClassLoader.php
  CLASS STRUCTURE:
    a cLibrary object:
      * is defined within a single file (currently @lib.php)
      * is a named set of cModules
      * contains one or more cModule objects
      * has a BasePath, to which all Module files are relative
    a cModule object:
      * is a collection of classes
      * handles autoloading of those classes
      * knows the filespec of each class's source file
*/
class cObjectResult {
    private $o = NULL;
    
    public function SetObject($o) {
	$this->o = $o;
    }
    public function GetObject() {
	return $this->o;
    }
    public function HasObject() {
	return !is_null($this->o);
    }
}
/*::::
  PURPOSE: static class to handle master lists
*/
class cLoader {

    // ++ SETUP ++ //

    static private $isSetup = FALSE;
    static protected function NeedSetup() { return !self::$isSetup; }
    static protected function MarkAsSetup() { self::$isSetup = TRUE; }
    /*----
      PURPOSE: configures the PHP autoloader to use this class
    */
    static protected function Init() {
        if (self::NeedSetup()) {
            self::AddDebugLine('INITIALIZING MOD LOADER');
            spl_autoload_register(__CLASS__ . '::LoadClass');
            self::MarkAsSetup();
        }
    }
    
    static private $fpBase = NULL;
    static public function SetBasePath(string $fpBase) {
        self::$fpBase = $fpBase;
        self::AddDebugLine('SETTING LOADER BASE PATH TO ['.self::GetBasePath().']');
    }
    // PUBLIC for cLibraryExec
    static public function GetBasePath() { return self::$fpBase; }
    static protected function HasBasePath() : bool { return is_string(self::$fpBase); }

    // -- SETUP -- //
    // ++ ACTIONS ++ //
    
    // API
    static public function LoadLibrary($sName) {
        $oStat = self::CheckLibrary($sName);
        if ($oStat->HasObject()) {
            $oStat->GetObject()->Load();
        } else {
            throw new \exception('Library "'.$sName.'" has not been registered.');
        }
    }
    
    // -- ACTIONS -- //
    // ++ INTERNAL DATA ++//

    static private $arLibs=array();		// list of modules
    static public function AddLibrary(cLibraryBase $oLib) {
        self::Init();	// make sure this class is set up to handle class autoloading
        $sName = $oLib->GetLibName();
        self::$arLibs[$sName] = $oLib;
    }
    static protected function CheckLibrary($sName) {
        $oStat = new cObjectResult();
        if (array_key_exists($sName,self::$arLibs)) {
            $oStat->SetObject(self::$arLibs[$sName]);
        }
        return $oStat;
    }
    
    static private $arClasses=array();	// index of classes: arCls[class] => module name
    static public function AddClass(cModule $oMod, $sName) {
        if (array_key_exists($sName,self::$arClasses)) {
            $sModNew = $oMod->GetName();
            $sModOld = self::$arClasses[$sName]->GetName();
            echo "<b>ClassLoader</b> error: class [$sName] already added by module [$sModOld], duplicate request from module [$sModNew].";
            echo '<pre>';
            throw new \exception('ClassLoader usage error: duplicate class registration.');
        } else {
            self::$arClasses[$sName] = $oMod;
            cLoader::AddDebugLine("ADDING CLASS [$sName] in file [".$oMod->GetAbsoluteSpec().']');
        }
    }
    static protected function CheckClass($sName) {
        $oStat = new cObjectResult();
        if (array_key_exists($sName,self::$arClasses)) {
            $oStat->SetObject(self::$arClasses[$sName]);
        }
        return $oStat;
    }
    static protected function GetClassDump() { return print_r(self::$arClasses,TRUE); }
    /*----
      CALLBACK via spl_autoload_register()
    */
    static public function LoadClass($sName) {
        $doDbg = self::GetDebugMode();
        self::AddDebugLine('LOADING CLASS: <b>'.$sName.'</b> {');
        $oStat = self::CheckClass($sName);
        if ($oStat->HasObject()) {
            $oMod = $oStat->GetObject();
            self::IncDebugDepth();
            $ok = $oMod->Load();
            if ($ok) {
                $sFnd = NULL;
                if (class_exists($sName)) {
                    $sFnd = 'class';
                } elseif (trait_exists($sName)) {
                    $sFnd = 'trait';
                }
                $sStat = 'found '.$sFnd;
                if (is_null($sFnd)) {
                    $sStat .= " but <span style='color:#ff0000;'>entity [$sName] still not found!</span>";
                }
            } else {
                $sStat = 'ERROR';
            }
            self::DecDebugDepth();
            self::AddDebugLine('}: '.$sStat);
            return TRUE;
        } else {
            if ($doDbg) {
                echo ' - class unknown. Available classes:<br><pre>';
                echo self::GetClassDump();
                throw new \exception("\n".'Request for unknown class "'.$sName.'".');
            } else {
                return FALSE;
            }
        }
    }

    // -- INTERNAL DATA --//
    // ++ DEBUGGING ++ //

    static private $doDebug = FALSE;
    static public function SetDebugMode(bool $iOn) { self::$doDebug = $iOn; }
    static public function GetDebugMode() : bool { return self::$doDebug; }
    static public function AddDebugLine(string $sLine,string $sEnd='<br>') {
        if (self::GetDebugMode()) {
            $out = str_repeat(' -',self::GetDebugDepth());
            echo $out.$sLine.$sEnd;
        }
    }
    
    static private $nDebugDepth = 0;
    static protected function IncDebugDepth() { self::$nDebugDepth++; }
    static protected function DecDebugDepth() { self::$nDebugDepth--; }
    static protected function GetDebugDepth() : int { return self::$nDebugDepth; }

    // -- DEBUGGING -- //
}
/*::::
  PURPOSE: manages collections of class references, which are registered with cLoader
    for autoloading when needed.
  IMPLEMENTATION: This abstract base class does not define how the definitions are accessed,
  HISTORY:
    2019-11-09 created so as to better serve the Dropins subsystem
*/
abstract class cLibraryBase {

    // ++ PROPERTIES ++ //

    abstract public function GetFileSpec();
    /*----
      PROPERTY: BasePath
      PURPOSE:  an optional string that is added to the beginning of index file specs.
	It defaults to the index file's folder, but sometimes we want to
	access paths outside of that. 
      HISTORY:
	2019-09-19 written
    */
    private $fpBase = NULL;
    public function SetBasePath(string $fpFolder) { $this->fpBase = $fpFolder; }
    /*----
      NOTE (2019-11-10): ->fpBase can be null when called from cModule::SetLibrary()
      HISTORY:
        2019-11-11 Renamed from GetBasePath() to GetLibraryBasePath() to make it easier to find calls
	  (because of the same name being used for methods in other classes).
        2019-11-24 Apparently fpBase *is* null for dropins. Without stopping to fully analyze the situation,
	  I'm just going to use dirname(GetFileSpec()) as the fallback.
    */
    public function GetLibraryBasePath() {
        if (is_null($this->fpBase)) {
            $fs = $this->GetFileSpec();
            $fp = dirname($fs);
            //echo 'FILE SPEC IS ['.$this->GetFileSpec().']<br>';
            //return $this->GetFolderSpec();
            return $fp;
        } else {
            return $this->fpBase;
        }
    }

    private $sName;
    protected function SetLibName(string $sName) { $this->sName = $sName; }
    // PUBLIC for cLoader
    public function GetLibName() : string { return $this->sName; }

    private $isLoaded = FALSE;
    protected function MarkAsLoaded() { $this->isLoaded = TRUE; }
    protected function GetIsLoaded() : bool { return $this->isLoaded; }

    // -- PROPERTIES -- //
    // ++ ACTIONS ++ //

    /*----
      API
      PUBLIC for loader class to use
      ACTION: loads the library's class reference index
        This does not load the classes themselves, but merely lets the class autoloader know
        where to find the classes when they are needed. This reduces the loading of
        unneeded libraries without the caller having to know where the libraries are located.
    */
    public function Load() {
        if (!$this->GetIsLoaded()) {
            $ok = $this->LoadNow();
            if ($ok) {
                $this->MarkAsLoaded();
            }
        }
    }
    /*----
      ACTION: unconditionally load the library
      HISTORY:
        2019-11-09 split into Load() and LoadNow() to make loading implementation simpler
    */
    abstract protected function LoadNow();

    /*----
      API
      USED BY: library indexes
      INPUT:
        $fs = relative path to PHP file which defines one or more registered classes
    */
    public function MakeModule(string $fs) : cModule { return new cModule($this,$fs); }

    // -- ACTIONS -- //

}
/*::::
  IMPLEMENTATION: This class loads executable library indexes (typically @lib.php) which are
    executed directly in order to register their classes.
*/
class cLibraryExec extends cLibraryBase {
    private static $arLibs;	// list of libraries

    // ++ SETUP ++ //

    /*----
      INPUT:
        $sName = name of library -- is used later to invoke it
        $fsIndex = relative spec to index file (typically "<relative path>/@libs.php")
          <relative path> is relative to $fp
        $fpBase (optional) = folder path with no trailing slash - just pass __DIR__ as the argument
      HISTORY:
        2019-11-03 Removed $fpFolder parameters in favor of [S/G]etBasePath().
        2019-11-09 Changing call-structure so we now *do* create libraries directly,
          to allow for different Library classes (to allow for different loading methods,
          mainly Dropins)
    */
    public function __construct(string $sName,string $fsIndex,$fpBase=NULL) {
        $this->SetLibName($sName);
        if (is_null($fpBase)) {
            $fpBase = cLoader::GetBasePath();
        }
        $this->SetBasePath($fpBase);
        $this->SetFileSpec($fpBase.'/'.$fsIndex);
        cLoader::AddLibrary($this);
        cLoader::AddDebugLine('New library: '.$sName);
    }

    private $fsIndexFile;
    protected function SetFileSpec(string $fs) {
        if (!file_exists($fs)) {
            $sName = $this->GetLibName();
            echo '<pre>';
            throw new \exception("ClassLoader internal error: Could not create library '$sName' because the file '$fs' does not seem to exist.");
        }
        $this->fsIndexFile = $fs;
    }
    // PUBLIC for debugging
    public function GetFileSpec() : string { return $this->fsIndexFile; }
    public function GetFolderSpec() : string { return dirname($this->GetFileSpec()); }

    // -- SETUP -- //
    // ++ ACTIONS ++ //

    /*----
      API CEMENT
      PUBLIC for cLoader
      HISTORY:
        2019-10-29 $oLib no longer necessary (if it ever was); use $this instead.
          That makes it clearer that index (@lib.php) files are being included in an object method.
        2019-11-09 renamed from Load() to LoadNow() for rewrite
    */
    public function LoadNow() {
        $fs = $this->GetFileSpec();
        
        $sName = $this->GetLibName();
        cLoader::AddDebugLine("LOADING library [$sName] from $fs");

        if (file_exists($fs)) {
            $this->SetBasePath(dirname($fs));	// assume library files are in same folder
            require_once($fs);			// run the index file
            return TRUE;			// success, if we didn't crash while running $fs
        } else {
            throw new \exception('2017-01-05 This should never happen anymore, since filespecs are checked when the module object is created.');
        }
    }
    /*----
      ACTION: include another index file, possibly in another folder
        This is needed in order to allow cascading includes without
        subsequent includes at the same level getting their paths
        added to those of the prior includes.
      DETAILS: temporarily adds fsRelative to the current base path,
        so that modules created within the given index file will
        have that path, then removes it when done.
      FUTURE: If we need to include an absolute path (e.g. for including
        outside the current folder), add an optional $fpBase parameter.
    */
    public function IncludeIndex(string $fpRelative,string $fnIndex) {
        $fpLib = $this->GetLibraryBasePath();
        $fpNew = $fpLib.$fpRelative;
        $fpOld = $fpLib;
        $this->SetBasePath($fpNew);
        $fsIndex = $fpNew.'/'.$fnIndex;
        cLoader::AddDebugLine('INCLUDING ['.$fsIndex.']');;
        $oLib = $this;
        if (file_exists($fsIndex)) {
            require_once($fsIndex);
        } else {
            echo "<b>Error</b>: IncludeIndex() could not find a file.<br>RELATIVE PATH: [$fpRelative]<br>INDEX FILENAME: [$fnIndex]<pre>";
            throw new \exception("ClassLoader error: index file [$fsIndex] not found.");
        }
        $this->SetBasePath($fpOld);
    }

    // -- ACTIONS -- //
}
class cModule {
    private static $nDepth = 0;		// recursion depth for loading modules

    //private $fsCaller;	// filespec of caller

    // ++ SETUP ++ //

    /*----
      USAGE: new fcCodeModule(__FILE__, <module filespec>);
      HISTORY:
        2013-08-29 no longer has a keyname, so can no longer check for duplicates
    */
    public function __construct(cLibraryBase $oLib, string $fsRelativeSpec) {
        $this->SetLibrary($oLib);
        $this->SetRelativeSpec($fsRelativeSpec);
    }

    /*----
      PROPERTY: BasePath
      PURPOSE: The Library's base path can be changed in between creating different modules,
        because different sets of modules may be based in different folder-areas.
    */
    private $fpBase;
    protected function SetBasePath(string $fp) { $this->fpBase = $fp; }
    protected function GetBasePath() : string { return $this->fpBase; }
    
    private $oLib;
    protected function SetLibrary(cLibraryBase $oLib) {
        $this->oLib = $oLib;
        $this->SetBasePath($oLib->GetLibraryBasePath());
    }
    public function GetLibrary() : cLibraryBase { return $this->oLib; }
    
    private $fnModule;
    protected function SetRelativeSpec(string $fs) { $this->fnModule = $fs; }
    // PUBLIC for cLoader to access for debug trace
    public function GetRelativeSpec() : string { return $this->fnModule; }
    // PUBLIC for cLoader to access for debug trace
    public function GetAbsoluteSpec() : string {
        $fsRel = $this->fnModule;
        $oLib = $this->GetLibrary();
        $fpBase = $this->GetBasePath();
        $fsFull = $fpBase.'/'.$fsRel;
        return $fsFull;
    }
    
    private $isLoaded = FALSE;
    protected function MarkAsLoaded() { $this->isLoaded = TRUE; }
    protected function GetIsLoaded() : bool { return $this->isLoaded; }

    // -- SETUP -- //
    // ++ API ++ //

    /*----
      PURPOSE: unique identifier for this module
      NOTE: Although this currently aliases to GetRelativeSpec(), it should *not* be used
        as a backdoor method of getting the spec. If the spec is needed outside the object,
        the need should be documented first and then GetRelativeSpec() should be made public.
      HISTORY:
        2019-10-15 created -- apparently this existed before the rewrite
    */
    public function GetName() : string { return $this->GetRelativeSpec(); }

    // -- API -- //
    // ++ UTILITY ++ //
    
    /*----
      PURPOSE: like array_key_exists(), but parameters are in a sensible order,
        and doesn't choke if array is NULL or not set.
      NOTE:
        * We can't deprecate this and use fcArray::Exists(), because that's in a module.
        * However, it is currently only used by two other methods, one of which is deprecated,
          so it may soon make more sense to roll it back into that method rather than being
          a separate one.
    */
    static protected function ArrayHas(array $ar=NULL, string $sKey) : bool {
        if (is_null($ar)) {
            return FALSE;
        } else {
            return array_key_exists($sKey,$ar);
        }
    }
    
    // -- UTILITY -- //
    // ++ CLASS MANAGEMENT ++ //

    public function AddClass($sName) { cLoader::AddClass($this,$sName); }

    // -- CLASS MANAGEMENT -- //
    // ++ MODULE MANAGEMENT ++ //

    /*----
      PUBLIC for cLoader
      ACTION: Loads the code pointed to by the current Module object
      NOTES:
        * We want to prevent modules from being loaded more than once, but it's not an error
          if the same module is requested more than once.
          Multiple plugins or independent libraries may be dependent on the same module.
    */
    public function Load() : bool {
        if ($this->GetIsLoaded()) {
            $ok = TRUE;
            // module already loaded, no action needed
        } else {
            $ok = FALSE;
            if (defined('KF_CLI_MODE')) {
                $ftHilitePfx = '[';
                $ftHiliteSfx = ']';
                $ftNewline = "\n";
            } else {
                $ftHilitePfx = '<b>';
                $ftHiliteSfx = '</b>';
                $ftNewline = '<br>';
            }
            try {
                $fsMod = $this->GetAbsoluteSpec();
                $sName = $this->GetRelativeSpec();
                if (file_exists($fsMod)) {
                    // 2019-11-13 It's possible we might want to chdir(dirname($fsMod)) so includes/requires will be relative to it
                    require_once $fsMod;
                    $ok = TRUE;
                    $this->MarkAsLoaded();
                    cLoader::AddDebugLine("Module $ftHilitePfx$sName$ftHiliteSfx loaded.");
                } else {
                    $oLib = $this->GetLibrary();
                    $sLib = $oLib->GetLibName();
                    $fsLib = $oLib->GetFileSpec();
                    //$intCaller = $this->intCaller;
                    echo "Module $ftHilitePfx$sName$ftHiliteSfx"
                      ." could not be loaded because source file $ftHilitePfx$fsMod$ftHiliteSfx,"
                      ." registered in $ftHilitePfx$sLib$ftHiliteSfx ($fsLib),"
                      ." is not found.$ftNewline";
                }
            } catch(Exception $e) {
                echo "ModLoader could not load module [$sName] from [$fsMod]; error: <b>".$e->getMessage().'</b>';

                throw new \exception('Module file could not be loaded.');
            }
        }
        return $ok;
    }

    // -- MODULE MANAGEMENT -- //
    
}
